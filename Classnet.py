import torch
import torch.nn as nn
import numpy as np
import sys
import torch.nn.functional as F

class CLASSNET12896(nn.Module):
    def __init__(self):
        super(CLASSNET12896, self).__init__()
        self.conv1 = nn.Sequential(
                nn.Conv2d(
                    in_channels = 3,
                    out_channels = 64,
                    kernel_size = (1, 3),
                    stride = (1,1),
                    padding = (0, 1)),
                nn.BatchNorm2d(64),
                nn.ReLU(),
                
                nn.Conv2d(64,64,(1,3),(1,1),(0,1)),
                nn.BatchNorm2d(64),
                nn.ReLU(),
                
                nn.MaxPool2d(2)
                )
        
        self.conv2 = nn.Sequential(
                nn.Conv2d(64, 128, (1,3), (1,1), (0, 1)),
                nn.BatchNorm2d(128),
                nn.ReLU(),

                nn.Conv2d(128, 128, (1,3),(1,1),(0,1)),
                nn.BatchNorm2d(128),
                nn.ReLU(),

                nn.MaxPool2d(2)
                )
                
                
        self.conv3 = nn.Sequential(
                nn.Conv2d(128, 256, (1,3), (1,1), (0,1)),
                nn.BatchNorm2d(256),
                nn.ReLU(),

                nn.Conv2d(256, 256, (1, 3),(1,1),(0,1)),
                nn.BatchNorm2d(256),
                nn.ReLU(),

                nn.Conv2d(256, 256, (1, 3),(1,1),(0,1)),
                nn.BatchNorm2d(256),
                nn.ReLU(),

                nn.MaxPool2d(2)
                )
        
        self.conv4 = nn.Sequential(
                nn.Conv2d(256, 512, (1,3), (1,1), (0,1)),
                nn.BatchNorm2d(512),
                nn.ReLU(),

                nn.Conv2d(512, 512, (1,3), (1,1), (0,1)),
                nn.BatchNorm2d(512),
                nn.ReLU(),

                nn.Conv2d(512, 512, (1,3), (1,1), (0,1)),
                nn.BatchNorm2d(512),
                nn.ReLU(),
                
                nn.MaxPool2d(2)
                )
                
        self.conv5 = nn.Sequential(
                nn.Conv2d(512, 1024, (1,3), (1,1), (0,1)),
                nn.BatchNorm2d(1024),
                nn.ReLU(),

                nn.Conv2d(1024, 1024, (1,3), (1,1), (0,1)),
                nn.BatchNorm2d(1024),
                nn.ReLU(),

                nn.Conv2d(1024, 1024, (1,3), (1,1), (0,1)),
                nn.BatchNorm2d(1024),
                nn.ReLU(),
                
                nn.MaxPool2d(2)
                )
        self.conv6 = nn.Sequential(
                nn.Conv2d(1024, 2048, 1, 1, 0),
                nn.Conv2d(2048, 2661, 1, 1, 0),
                )
        #self.conv7 = nn.AvgPool2d(kernel_size=(3,4), stride=1,padding=0)
        self.conv7 = nn.AdaptiveAvgPool2d((1, 1))

    def forward(self, x):
        x = self.conv1(x)
        x = self.conv2(x)
        x = self.conv3(x)
        x = self.conv4(x)
        x = self.conv5(x)
        x = self.conv6(x)
        x = self.conv7(x)
        x = x.squeeze()
        out = F.softmax(x, dim = 1)
        return out

if __name__ =='__main__':

    net = CLASSNET12896()
    net.cuda()
    a = torch.ones((12,3,128,96), dtype = torch.float)
    a = a.cuda()
    out = net.forward(a)
    print(out.shape)
    

