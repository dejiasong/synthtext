import torch
import os
import sys
import time
import torch.nn.functional as F
from torchvision import datasets, transforms, utils
import torchvision
from  Classnet import CLASSNET12896 
import transform 
import numbers
from  transform import get_transform

# we should resize
img_dir_path = '/home/djsong/dataset/train/'
save_dir_path = '/home/djsong/workspace/synthtext/classifier/weights'
log = '/home/djsong/workspace/synthtext/classifier/log'
BSIZE = 128
EPOCH = 10
LR = 0.01
class Option(object):
    def __init__(self, t_size, resize_or_crop='scale_width_and_crop_to_designated_size'):
        if isinstance(t_size, numbers.Number):
            self.size = (int(t_size), int(t_size))
        else:
            self.size = t_size

        self.image_width, self.image_height = self.size
        self.resize_or_crop=resize_or_crop
opt = Option((128, 96),resize_or_crop='scale_width_and_crop_to_designated_size')
    
def make_weights_for_balanced_classes(images, nclasses):
    """
    Argumentation:
    images  -- a list contain ['path/to/every/img', class_idex] ex.[path, 0]
    nclasses-- a realvalue classes for all images
    
    Note:a map class, item , count
    """
    count = [0] * nclasses
    #compute num of img in every class
    for item in images:
        count[item[1]] += 1
    
    # compute weight for each class
    weight_per_class = [0.] * nclasses
    N = float(sum(count))#classnum:2661  N:755714
    for i in range(nclasses):
        weight_per_class[i] = N/float(count[i])

    # compute weigt for every image
    # this equal to class weights
    # more imgs less weights
    weight = [0]*len(images)
    for idx, val in enumerate(images):
        weight[idx] = weight_per_class[val[1]]
    
    return weight

def load_npz(weightfile):
    pass


def main():
    """
    prepare data
    """

    # use torchvision to create a obj about img_dir
    transform = get_transform(opt)
    dataset_train = datasets.ImageFolder(img_dir_path, transform = transform)
    weights = make_weights_for_balanced_classes(dataset_train.imgs, len(dataset_train.classes)) # for every images
    weights = torch.DoubleTensor(weights)

    # we will re-sample by using weights
    # weights is a weight listfor every img
    sampler = torch.utils.data.sampler.WeightedRandomSampler(weights, len(weights)) 

    #dataloader need a tensor
    train_loader = torch.utils.data.DataLoader(dataset_train,batch_size=BSIZE,sampler= sampler,pin_memory=True) 
    
    print("load data done ...""")
    """
    net
    """
    net = CLASSNET12896()
    net = net.cuda()
    print("load net  done ...""")

    
    
    """
    load parameters
    """

    #net.load_state_dict(torch.load('net_params'))
    print("load params done ...""")

    
    
    """
    optimizer
    """
    optimizer = torch.optim.Adam(net.parameters(), lr=LR)
    print("load optimizer done ...")

    """
    train
    """
    # write log
    log_path = os.path.join(log, 'loss.txt')
    os.remove(log_path)
    if not os.path.exists(log_path):
        os.mknod(log_path)
    
    count = 0
    for epoch in range(EPOCH):
        t_e_s = time.time()
        loss_a = 0
        for i, (data, label) in enumerate(train_loader):#(bsize, 3, h,w) (bsize)
            t_start = time.time()
            data_, label_ = data.cuda(), label.cuda()

            out_ = net.forward(data_)
            loss_ = F.cross_entropy(out_, label_)
            optimizer.zero_grad()
            loss_.backward()
            optimizer.step()
            t = time.time() - t_start
            count+=1
            loss_a += loss_.cpu()
            print("Epoch {} batch {} loss {:.6f} time {:.2f}ms".format(epoch, i, loss_.cpu(),t*1000))
            
            if count%50 ==0 and count!=0:
                with open(log_path, 'a') as f:
                    f.write('iteration:{} loss:{:.6f} time:{:.2f}s\n'.format(count, loss_.cpu(),t*50))
        t_e = time.time() - t_e_s
        # every epoch: 
        with open(log_path, 'a') as f:
            f.write('#####EPOCH:{} loss:{:.2f}  time:{:.2f} s\n'.format(epoch, loss_a*(epoch+1)/count, t_e))

        save_name = os.path.join(save_dir_path,'_{}.pkl'.format(count))
        troch.save(net.state_dict(), save_name)

     
    
if __name__ == '__main__':
    main()
