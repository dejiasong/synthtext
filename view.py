import cv2
import os
import time
import sys
path = '/home/djsong/dataset/train/'
for root, dirs, files in os.walk(path):
    if len(dirs) == 0:
        for filename in files:
            classname = root.split('/')[-1]
            img_path = os.path.join(root, filename)
            img = cv2.imread(img_path)
            #cv2.imshow(classname, img)
            #cv2.waitKey(0)
            if img.shape[0] != 96 or img.shape[1] != 128:
                print('class:',img_path, 'img:',img.shape)
print('done')
